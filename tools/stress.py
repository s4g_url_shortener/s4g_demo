from locust import HttpUser, between, task

from random import choice
from string import hexdigits

def random_url():
    return f"https://{''.join(map(lambda _: choice(hexdigits), range(30)))}.com"

class NormalUser(HttpUser):
    wait_time = between(1, 4)

    @task(360)
    def index_providers(self):
        self.client.get("/api/v1/provider")

    @task(1)
    def shorten_bitly_correct_usage(self):
        self.client.put("/api/v1/shorten", json={ "url": random_url(), "provider": "bitly"})

    @task(39)
    def shorten_tinyurl_correct_usage(self):
        self.client.put("/api/v1/shorten", json={ "url": random_url(), "provider": "tinyurl"})

