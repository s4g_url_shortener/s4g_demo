# S4G Url Shortener Demo

This repository maintains a [Docker Compose](https://docs.docker.com/compose/) demo that automatically starts:
- A [url shortening service](https://gitlab.com/s4g_url_shortener/s4g_backend) instance.
- A [url shortening admin panel](https://gitlab.com/s4g_url_shortener/s4g_frontend) instance.
- An [OpenTelemetry Collector](https://opentelemetry.io/docs/collector/) instance which collects trace and metrics from the above services and then exports them to
[Prometheus](https://prometheus.io/) for metrics processing and alerting, and to [Jaeger](https://www.jaegertracing.io/) for tracing processing and visualization.
- A [Prometheus](https://prometheus.io/) instance.
- A [Jaeger](https://www.jaegertracing.io/) instance.
- A [Grafana](https://grafana.com/) instance for providing a better UI for Prometheus metrics and alerts.
- A [Locust](https://locust.io/) master node for performing light stress testing in order to produce some meaningful traces and metrics for visualization.

# Components

## [Url Shortening Service](https://gitlab.com/s4g_url_shortener/s4g_backend) - http://localhost:8081

![image](/uploads/fdbf9291cf5347bc62c85b7ed855c189/image.png)

The main shortening, service. See the repo for concrete examples.

## Url Shortening Service Swagger UI - http://localhost:8081/swagger-ui/

![image](/uploads/5dd859119b60c92ce3972f0e886bc483/image.png)

The embedded [Swagger UI](https://swagger.io/tools/swagger-ui/) instance can be used to visualize the REST Api and retrieve the [OpenAPI](https://swagger.io/specification/) definitions.

## [Url Shortening Admin Panel](https://gitlab.com/s4g_url_shortener/s4g_frontend)  - http://localhost:8080

![image](/uploads/b46de612776b54285d0b8f2283969fb1/image.png)

Can be used to retrieve Provider reports as well as enable or disable them.

## Jaeger - http://localhost:16686

![image](/uploads/d73d555a33c23712865a77d9e1980131/image.png)

Jaeger is an open source tool for querying and visualizing distributed traces. The above image demonstrated distributed tracing between the
admin panel and the URL shortening service.

## Prometheus - http://localhost:9090

![image](/uploads/912f9d70b7346bbabd08be89a7da1a3c/image.png)

Prometheus is a systems monitoring and alerting toolking that can be used to process our metrics and set configurable alerts.

## Grafana - [http://localhost:3000](http://localhost:3000/d/dlx__KDVk/s4g-backend?orgId=1&refresh=5s)

![image](/uploads/dbf0b5de6e922bf10e44bcea7c37ee2b/image.png)

Grafana is used as a visualization frontend to Prometheus.
It can also be used separately for alerts while integrating metrics from other platforms.

username: admin
password: password

# Configuration

The following environment variables must be passed to the backend service.

- `S4G__BITLY__BEARER`: The [Bitly access token](https://dev.bitly.com/docs/getting-started/authentication/)
- `S4G__TINYURL__BEARER`: and [Tinyurl access token](https://tinyurl.com/app/dev)

We provide default tokens for evaluation purposes. If the credits run out, new access tokens can be
provided by changing the appropriate variables in the `docker-compose.yml` file.

# Running

In order to execute the demo only [Docker Compose](https://docs.docker.com/compose/) is required.

Execute:
- `docker compose up -d`: At the project root to start the services
- `docker compose down`:  To destroy them. 
